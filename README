================
libgsecuredelete
================

libgsecuredelete is a GObject wrapper library for the secure-delete
tools (srm, sfill, sswap and smem), aiming to ease use of these tools
from programs by providing a simple but complete API to invoke them.

Build dependencies
==================

To build this library you will need:

* A C compiler (such as gcc)
* make
* autoconf
* libtool
* pkg-config
* GLib 2 (libglib2.0, some distributions might have a separate
  libgobject2.0)

If you want to build the development version, you will need
valac >= 0.14.2.  Note that the current build system cannot work
correctly if a Vala compiler is installed but at a too old version, even
though having no Vala compiler at all is perfectly fine when building
from tarballs.

Dependencies
============

In addition to the build dependencies, you will need the following
extra packages for the library to work properly:

* secure-delete
* pkexec (from the policykit package)

Quick installation instructions
===============================

::

  $ ./configure
  $ make
  # make install

For more detailed instructions and options, read INSTALL.
